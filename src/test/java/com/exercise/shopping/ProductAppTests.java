package com.exercise.shopping;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import static com.exercise.shopping.helper.JsonHelper.productJson;
import static org.hamcrest.Matchers.is;

@SpringBootTest
@AutoConfigureMockMvc
class ProductAppTests {

    @Autowired
    private MockMvc mockMvc;

    private String productNameCre = "First";
    private int productPriceCre = 101;

    private String productNameUpd = "First(updated)";
    private int productPriceUpd = 291;

    private String productJsonCre = productJson(productNameCre, productPriceCre);
    private String productJsonUpd = productJson(productNameUpd, productPriceUpd);

    /* create -> get -> list -> update -> delete -> list */

    @Test
    void shouldExecuteSequenceOfOperationsSuccessfully() throws Exception {

        // first product creation must be successful
        String strId = mockMvc.perform(post("/products")
                .content(productJsonCre)
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();

        int id = Integer.parseInt(strId);

        // retrieval by product ID must be successful
        mockMvc.perform(get("/products/" + id)
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(id)))
                .andExpect(jsonPath("$.name", is(productNameCre)))
                .andExpect(jsonPath("$.price", is(productPriceCre)));

        // products list must contain 1 element
        mockMvc.perform(get("/products")
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.length()", is(1)))
                .andExpect(jsonPath("$[0].id", is(id)))
                .andExpect(jsonPath("$[0].name", is(productNameCre)))
                .andExpect(jsonPath("$[0].price", is(productPriceCre)));

        // update must be successful
        mockMvc.perform(put("/products/" + id)
                .content(productJsonUpd)
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(id)))
                .andExpect(jsonPath("$.name", is(productNameUpd)))
                .andExpect(jsonPath("$.price", is(productPriceUpd)));

        // delete must be successful
        mockMvc.perform(delete("/products/" + id)
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());

        // products list must be empty
        mockMvc.perform(get("/products")
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.length()", is(0)));
    }

}
