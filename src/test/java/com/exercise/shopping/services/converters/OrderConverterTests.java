package com.exercise.shopping.services.converters;

import com.exercise.shopping.dto.OrderDto;
import com.exercise.shopping.entities.Order;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

class OrderConverterTests {

    @Test
    void shouldConvertToCorrectDto() {

        Order order = spy(new Order(101,
                "Order",
                new HashSet<>()));

        when(order.getTimestampPlacement())
                .thenReturn(3000089L);

        when(order.retrieveProductIds())
                .thenReturn(Arrays.asList(300L, 500L, 700L));

        OrderDto dto = new OrderConverter().toDto(order);

        assertEquals(101L, dto.getId());
        assertEquals("Order", dto.getEmailBuyer());
        assertEquals(3000089L, dto.getTimestampPlacement());

        assertTrue(dto.getProducts().containsAll(Arrays.asList(300L, 500L, 700L)));

    }

}
