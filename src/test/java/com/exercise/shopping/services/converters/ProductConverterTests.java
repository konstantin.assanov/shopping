package com.exercise.shopping.services.converters;

import com.exercise.shopping.dto.ProductDto;
import com.exercise.shopping.entities.Product;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

class ProductConverterTests {

    @Test
    void shouldConvertToCorrectDto() {

        Product product = spy(new Product(101,
                "Product",
                30017,
                true));

        when(product.getTimestampCreation())
                .thenReturn(3000089L);

        ProductDto dto = new ProductConverter().toDto(product);

        assertEquals(101L, dto.getId());
        assertEquals("Product", dto.getName());
        assertEquals(30017, dto.getPrice());
        assertEquals(3000089L, dto.getTimestampCreation());
    }

}
