package com.exercise.shopping.entities;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class OrderTests {

    @Test
    void shouldBeCorrectOrderProductsSum() {

        Set<Product> products = new HashSet<>();
        products.add(buildProductMockWithPrice(101));
        products.add(buildProductMockWithPrice(17));
        products.add(buildProductMockWithPrice(405));
        products.add(buildProductMockWithPrice(230));

        Order order = new Order(101, "email@hotmail.com", products);

        assertEquals(753, order.calculateTotalPrice());

    }

    private Product buildProductMockWithPrice(int price) {
        Product product = mock(Product.class);
        when(product.getPrice()).thenReturn(price);
        return product;
    }

    @Test
    void shouldBeCorrectListOfProductIds() {

        Set<Product> products = new HashSet<>();
        products.add(buildProductMockWithId(101));
        products.add(buildProductMockWithId(17));
        products.add(buildProductMockWithId(405));
        products.add(buildProductMockWithId(230));

        Order order = new Order(101, "email@hotmail.com", products);

        assertTrue(order.retrieveProductIds().containsAll(Arrays.asList(101L, 17L, 405L, 230L)));
    }

    private Product buildProductMockWithId(long id) {
        Product product = mock(Product.class);
        when(product.getId()).thenReturn(id);
        return product;
    }

}