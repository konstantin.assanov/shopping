package com.exercise.shopping.helper;

import java.util.List;

public class JsonHelper {

    public static String productJson(String name, int price) {
        return ("{ 'name': '" + name + "', 'price': " + price + " }")
                .replaceAll("'", "\"");
    }

    public static String orderJson(String emailBuyer, List<String> productIds) {
        return ("{ 'emailBuyer' : '" + emailBuyer + "', 'products': [" + String.join(",", productIds) + "] }")
                .replaceAll("'", "\"");
    }

}