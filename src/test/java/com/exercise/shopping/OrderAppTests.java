package com.exercise.shopping;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.Date;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import static com.exercise.shopping.helper.JsonHelper.productJson;
import static com.exercise.shopping.helper.JsonHelper.orderJson;

@SpringBootTest
@AutoConfigureMockMvc
class OrderAppTests {

    @Autowired
    private MockMvc mockMvc;

    private String productNameCre = "First";
    private int productPriceCre = 101;

    private String productNameUpd = "First(updated)";
    private int productPriceUpd = 291;

    private String productJsonCre = productJson(productNameCre, productPriceCre);
    private String productJsonUpd = productJson(productNameUpd, productPriceUpd);

    /* create product #1 -> create product #2 -> create order -> list all orders -> list orders for a period */

    @Test
    void shouldExecuteSequenceOfOperationsSuccessfully() throws Exception {

        // product #1 creation must be successful
        String strCreId = mockMvc.perform(post("/products")
                .content(productJsonCre)
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();

        // product #2 creation must be successful
        String strUpdId = mockMvc.perform(post("/products")
                .content(productJsonUpd)
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();

        long timeBefore = new Date().getTime();

        // order creation must be successful
        String strOrderId = mockMvc.perform(post("/orders")
                .content(orderJson("email@email.com", Arrays.asList(strCreId, strUpdId)))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();

        int orderId = Integer.parseInt(strOrderId);

        long timeAfter = new Date().getTime();

        // all orders list must contain 1 element
        mockMvc.perform(get("/orders")
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.length()", is(1)))
                .andExpect(jsonPath("$[0].id", is(orderId)))
                .andExpect(jsonPath("$[0].emailBuyer", is("email@email.com")))
                .andExpect(jsonPath("$[0].products.length()", is(2)))
                .andExpect(jsonPath("$[0].totalPrice", is(productPriceCre + productPriceUpd)));

        // orders between the dates must contain 1 element
        mockMvc.perform(get("/orders/between")
                .queryParam("from", "" + timeBefore)
                .queryParam("to", "" + timeAfter)
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.length()", is(1)))
                .andExpect(jsonPath("$[0].id", is(orderId)))
                .andExpect(jsonPath("$[0].emailBuyer", is("email@email.com")))
                .andExpect(jsonPath("$[0].products.length()", is(2)))
                .andExpect(jsonPath("$[0].totalPrice", is(productPriceCre + productPriceUpd)));

        // remove all products
        mockMvc.perform(delete("/products/" + strCreId)
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());

        mockMvc.perform(delete("/products/" + strUpdId)
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

}