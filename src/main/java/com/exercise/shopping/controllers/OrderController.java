package com.exercise.shopping.controllers;

import com.exercise.shopping.dto.OrderDto;
import com.exercise.shopping.dto.OrderRequestDto;
import com.exercise.shopping.services.IOrderService;

import org.springframework.stereotype.Component;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/orders")
@Component
public class OrderController {

    private final IOrderService orderService;

    OrderController(IOrderService orderService) {
        this.orderService = orderService;
    }

    @GetMapping()
    public List<OrderDto> orders() {
        return orderService.list();
    }

    @GetMapping("/between")
    public List<OrderDto> ordersBetweenDates(
            @RequestParam(name = "from") long from,
            @RequestParam(name = "to") long to) {
        return orderService.listBetweenDates(from, to);
    }

    @PostMapping()
    public long create(@RequestBody OrderRequestDto dto) {
        return orderService.create(dto);
    }

}