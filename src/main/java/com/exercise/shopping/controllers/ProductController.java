package com.exercise.shopping.controllers;

import com.exercise.shopping.services.IProductService;
import com.exercise.shopping.dto.ProductDto;
import com.exercise.shopping.dto.ProductRequestDto;

import org.springframework.stereotype.Component;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/products")
@Component
public class ProductController {

    private final IProductService productService;

    ProductController(IProductService productService) {
        this.productService = productService;
    }

    @GetMapping()
    public List<ProductDto> products() {
        return productService.list();
    }

    @GetMapping("/{id}")
    public ProductDto product(@PathVariable("id") long id) {
        return productService.get(id);
    }

    @PostMapping()
    public long create(@RequestBody ProductRequestDto dto) {
        return productService.create(dto);
    }

    @PutMapping("/{id}")
    public ProductDto update(@PathVariable("id") long id, @RequestBody ProductRequestDto dto) {
        return productService.update(id, dto);
    }

    @DeleteMapping("/{id}")
    public boolean delete(@PathVariable("id") long id) {
        return productService.deleteSoft(id);
    }

}