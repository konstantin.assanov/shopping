package com.exercise.shopping.store.impl;

import com.exercise.shopping.entities.Product;
import com.exercise.shopping.store.IProductStore;
import com.exercise.shopping.store.repository.IProductRepository;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
class ProductStoreImpl implements IProductStore {

    private final IProductRepository repo;

    ProductStoreImpl(IProductRepository repo) {
        this.repo = repo;
    }

    public Optional<Product> get(long id) {
        return Optional.ofNullable(repo.findByIdAndIsDeleted(id, false));
    }

    public List<Product> findAll() {
        final Iterable<Product> iterable = repo.findByIsDeleted(false);
        final List<Product> products = new ArrayList<>();
        iterable.forEach(products::add);
        return products;
    }

    public Product save(Product product) {
        return repo.save(product);
    }

    public Set<Product> resolveProductIds(List<Long> productIds) {
        return productIds
                .stream()
                .distinct()
                .map(this::get)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toSet());
    }

}