package com.exercise.shopping.store.impl;

import com.exercise.shopping.entities.Order;
import com.exercise.shopping.store.IOrderStore;
import com.exercise.shopping.store.repository.IOrderRepository;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
class OrderStoreImpl implements IOrderStore {

    private final IOrderRepository repo;

    OrderStoreImpl(IOrderRepository repo) {
        this.repo = repo;
    }

    public List<Order> findAll() {
        final Iterable<Order> iterable = repo.findAll();
        return toList(iterable);
    }

    public List<Order> findAllBetweenDates(Long from, Long to) {
        final Iterable<Order> iterable = repo.findByTimestampPlacementBetween(from, to);
        return toList(iterable);
    }

    private List<Order> toList(final Iterable<Order> iterable) {
        final List<Order> orders = new ArrayList<>();
        iterable.forEach(orders::add);
        return orders;
    }

    public Order save(Order order) {
        return repo.save(order);
    }

}