package com.exercise.shopping.store;

import com.exercise.shopping.entities.Product;

import java.util.List;
import java.util.Optional;
import java.util.Set;

public interface IProductStore {

    Optional<Product> get(long id);

    List<Product> findAll();

    Product save(Product product);

    Set<Product> resolveProductIds(List<Long> productIds);

}