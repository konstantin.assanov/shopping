package com.exercise.shopping.store;

import com.exercise.shopping.entities.Order;

import java.util.List;

public interface IOrderStore {

    List<Order> findAll();

    List<Order> findAllBetweenDates(Long from, Long to);

    Order save(Order order);

}