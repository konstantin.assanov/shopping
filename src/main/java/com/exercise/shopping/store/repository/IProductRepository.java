package com.exercise.shopping.store.repository;

import org.springframework.data.repository.CrudRepository;

import com.exercise.shopping.entities.Product;

public interface IProductRepository extends CrudRepository<Product, Long> {

    Product findByIdAndIsDeleted(Long id, boolean isDeleted);

    Iterable<Product> findByIsDeleted(boolean isDeleted);

}