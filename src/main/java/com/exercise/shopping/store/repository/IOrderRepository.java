package com.exercise.shopping.store.repository;

import org.springframework.data.repository.CrudRepository;

import com.exercise.shopping.entities.Order;

public interface IOrderRepository extends CrudRepository<Order, Long> {

    Iterable<Order> findByTimestampPlacementBetween(Long from, Long to);

}