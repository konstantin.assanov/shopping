package com.exercise.shopping.dto;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;

import java.util.List;

@Data
@Setter(AccessLevel.NONE)
public class OrderDto extends OrderRequestDto {

    private final long id;

    private final long timestampPlacement;

    private final Integer totalPrice;

    public OrderDto(long id,
                    String emailBuyer,
                    List<Long> products,
                    long timestampPlacement,
                    Integer totalPrice) {
        super(emailBuyer, products);
        this.id = id;
        this.timestampPlacement = timestampPlacement;
        this.totalPrice = totalPrice;
    }

}