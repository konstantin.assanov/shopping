package com.exercise.shopping.dto;

import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;

@Data
@NoArgsConstructor(force=true)
@Setter(AccessLevel.NONE)
public class ProductRequestDto {

    @NotBlank(message = "Product Name cannot be null or blank")
    @Size(min = 2, max = 128, message = "Product Name must be between 2 and 128 characters")
    private final String name;

    @NotNull(message = "Product Price cannot be null")
    @PositiveOrZero(message = "Product Price cannot be negative")
    private final int price; // price in cents

    ProductRequestDto(String name, int price) {
        this.name = name;
        this.price = price;
    }

}