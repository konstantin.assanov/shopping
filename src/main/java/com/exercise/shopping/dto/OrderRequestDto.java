package com.exercise.shopping.dto;

import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.List;

@Data
@NoArgsConstructor(force=true)
@Setter(AccessLevel.NONE)
public class OrderRequestDto {

    @NotBlank(message = "Buyer Email cannot be null or blank")
    @Email(message = "Email address must be of valid format")
    private final String emailBuyer;

    @NotEmpty(message = "Product list should contain at least 1 valid product Id.")
    private final List<Long> products;

    OrderRequestDto(String emailBuyer, List<Long> products) {
        this.emailBuyer = emailBuyer;
        this.products = products;
    }

}