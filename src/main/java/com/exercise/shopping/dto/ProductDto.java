package com.exercise.shopping.dto;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;

@Data
@Setter(AccessLevel.NONE)
public class ProductDto extends ProductRequestDto {

    private final long id;

    private final long timestampCreation;

    public ProductDto(long id, String name, int price, long timestampCreation) {
        super(name, price);
        this.id = id;
        this.timestampCreation = timestampCreation;
    }

}