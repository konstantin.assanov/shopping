package com.exercise.shopping.entities;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.Table;

import javax.persistence.Version;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Entity
@Table(name = "products")
public class Product {

    @Version
    private long version;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @NotBlank(message = "Product Name cannot be null or blank")
    @Size(min = 2, max = 128, message = "Product Name must be between 2 and 128 characters")
    private String name;

    @NotNull(message = "Product Price cannot be null")
    @PositiveOrZero(message = "Product Price cannot be negative")
    private int price;              // price in cents, not nullable because of a primitive 'int' type

    @PositiveOrZero(message = "Creation Timestamp cannot be negative")
    private long timestampCreation; // not nullable because of a primitive 'long' type

    private boolean isDeleted;      // not nullable because of a primitive 'boolean' type

    public Product(long id, String name, int price, boolean isDeleted) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.isDeleted = isDeleted;
    }

    @PrePersist
    void onPersist() {
        timestampCreation = new Date().getTime();
    }
}