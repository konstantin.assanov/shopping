package com.exercise.shopping.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;

import javax.persistence.Version;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Entity
@Table(name = "orders")
public class Order {

    @Version
    private long version;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @NotBlank(message = "Buyer Email cannot be null or blank")
    @Email(message = "Email address must be of valid format")
    private String emailBuyer;

    @PositiveOrZero(message = "Placement Timestamp cannot be negative")
    private long timestampPlacement;    // not nullable because of a primitive 'long' type

    @ManyToMany
    @JoinTable(name = "ordered_products",
            joinColumns = @JoinColumn(name = "order_id"),
            inverseJoinColumns = @JoinColumn(name = "product_id"))
    @NotEmpty(message = "Product list should contain at least 1 valid product Id.")
    private Set<@NotNull Product> products;

    public Order(long id,
                 String emailBuyer,
                 Set<Product> products) {
        this.id = id;
        this.emailBuyer = emailBuyer;
        this.products = products;
    }

    @PrePersist
    void onPersist() {
        timestampPlacement = new Date().getTime();
    }

    public int calculateTotalPrice() {
        return products
                .stream()
                .map(Product::getPrice)
                .reduce((a, b) -> {return a + b;})
                .orElse(0);
    }

    public List<Long> retrieveProductIds() {
        return products
                .stream()
                .map(Product::getId)
                .collect(Collectors.toList());
    }
}