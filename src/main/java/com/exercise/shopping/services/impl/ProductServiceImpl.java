package com.exercise.shopping.services.impl;

import com.exercise.shopping.exceptions.BadRequestException;
import com.exercise.shopping.exceptions.ResourceNotFoundException;
import com.exercise.shopping.entities.Product;
import com.exercise.shopping.services.IProductService;
import com.exercise.shopping.services.converters.ProductConverter;
import com.exercise.shopping.store.IProductStore;
import com.exercise.shopping.dto.ProductDto;
import com.exercise.shopping.dto.ProductRequestDto;
import com.exercise.shopping.validation.IBasicValidator;

import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
class ProductServiceImpl implements IProductService {

    private final IProductStore productStore;
    private final IBasicValidator<Product> productValidator;
    private final IBasicValidator<ProductRequestDto> dtoValidator;
    private final ProductConverter converter;

    public ProductServiceImpl(IProductStore productStore,
                              IBasicValidator<ProductRequestDto> dtoValidator,
                              IBasicValidator<Product> productValidator,
                              ProductConverter converter) {
        this.productStore = productStore;
        this.dtoValidator = dtoValidator;
        this.productValidator = productValidator;
        this.converter = converter;
    }

    public ProductDto get(long id) {
        final Optional<Product> optionalProduct = productStore.get(id);
        if (!optionalProduct.isPresent()) {
            throw new ResourceNotFoundException("Invalid Product ID: " + id);
        }

        return converter.toDto(optionalProduct.get());
    }

    public List<ProductDto> list() {
        return productStore.findAll()
                .stream()
                .map(converter::toDto)
                .collect(Collectors.toList());
    }

    public long create(ProductRequestDto dto) {
        dtoValidator.validate(dto);

        final Product product = new Product(0,
                dto.getName(),
                dto.getPrice(),
                false);

        productValidator.validate(product);

        try {
            final Product created = productStore.save(product);
            return created.getId();
        } catch(RuntimeException rte) {
            throw new BadRequestException(rte.getMessage());
        }
    }

    public ProductDto update(long id, ProductRequestDto dto) {
        dtoValidator.validate(dto);

        final Optional<Product> optionalProduct =  productStore.get(id);
        if (!optionalProduct.isPresent()) {
            throw new ResourceNotFoundException("Invalid Product ID: " + id);
        }

        final Product product = optionalProduct.get();

        product.setName(dto.getName());
        product.setPrice(dto.getPrice());

        productValidator.validate(product);

        try {
            return converter.toDto(productStore.save(product));
        } catch (RuntimeException rte) {
            throw new BadRequestException(rte.getMessage());
        }
    }

    public boolean deleteSoft(long id) {
        final Optional<Product> optionalProduct = productStore.get(id);
        if (!optionalProduct.isPresent()) {
            throw new ResourceNotFoundException("Invalid Product ID: " + id);
        }

        final Product product = optionalProduct.get();
        product.setDeleted(true);
        try {
            productStore.save(product);
        } catch (RuntimeException rte) {
            throw new BadRequestException(rte.getMessage());
        }
        return true;
    }

}