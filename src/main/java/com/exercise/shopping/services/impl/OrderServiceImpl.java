package com.exercise.shopping.services.impl;

import com.exercise.shopping.dto.OrderDto;
import com.exercise.shopping.dto.OrderRequestDto;
import com.exercise.shopping.entities.Order;
import com.exercise.shopping.entities.Product;
import com.exercise.shopping.exceptions.BadRequestException;
import com.exercise.shopping.services.IOrderService;
import com.exercise.shopping.services.converters.OrderConverter;
import com.exercise.shopping.store.IOrderStore;
import com.exercise.shopping.store.IProductStore;
import com.exercise.shopping.validation.IBasicValidator;

import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
class OrderServiceImpl implements IOrderService {

    private final IOrderStore orderStore;
    private final IProductStore productStore;
    private final IBasicValidator<OrderRequestDto> dtoValidator;
    private final IBasicValidator<Order> orderValidator;
    private final OrderConverter converter;

    public OrderServiceImpl(IOrderStore orderStore,
                            IProductStore productStore,
                            IBasicValidator<OrderRequestDto> dtoValidator,
                            IBasicValidator<Order> orderValidator,
                            OrderConverter converter) {
        this.orderStore = orderStore;
        this.productStore = productStore;
        this.dtoValidator = dtoValidator;
        this.orderValidator = orderValidator;
        this.converter = converter;
    }

    public List<OrderDto> list() {
        return orderStore.findAll()
                .stream()
                .map(converter::toDto)
                .collect(Collectors.toList());
    }

    public List<OrderDto> listBetweenDates(long from, long to) {
        return orderStore.findAllBetweenDates(from, to)
                .stream()
                .map(converter::toDto)
                .collect(Collectors.toList());
    }

    @Transactional
    public long create(OrderRequestDto dto) {
        dtoValidator.validate(dto);

        final Set<Product> products = productStore.resolveProductIds(dto.getProducts());

        final Order order = new Order(
                0,
                dto.getEmailBuyer(),
                products);

        orderValidator.validate(order);

        try{
            final Order created = orderStore.save(order);
            return created.getId();
        } catch(RuntimeException rte) {
            throw new BadRequestException(rte.getMessage());
        }
    }

}