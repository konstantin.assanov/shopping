package com.exercise.shopping.services;

import com.exercise.shopping.dto.ProductDto;
import com.exercise.shopping.dto.ProductRequestDto;

import java.util.List;

public interface IProductService {

    ProductDto get(long id);

    List<ProductDto> list();

    long create(ProductRequestDto dto);

    ProductDto update(long id, ProductRequestDto dto);

    boolean deleteSoft(long id);

}
