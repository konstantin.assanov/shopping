package com.exercise.shopping.services.converters;

import com.exercise.shopping.dto.ProductDto;
import com.exercise.shopping.entities.Product;

import org.springframework.stereotype.Service;

@Service
public class ProductConverter {

    public ProductDto toDto(Product product) {
        return new ProductDto(
                product.getId(),
                product.getName(),
                product.getPrice(),
                product.getTimestampCreation());
    }

}