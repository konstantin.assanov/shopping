package com.exercise.shopping.services.converters;

import com.exercise.shopping.dto.OrderDto;
import com.exercise.shopping.entities.Order;

import org.springframework.stereotype.Service;

@Service
public class OrderConverter {

    public OrderDto toDto(Order order) {
        return new OrderDto(
                order.getId(),
                order.getEmailBuyer(),
                order.retrieveProductIds(),
                order.getTimestampPlacement(),
                order.calculateTotalPrice());
    }

}