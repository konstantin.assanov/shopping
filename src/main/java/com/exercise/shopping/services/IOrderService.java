package com.exercise.shopping.services;

import com.exercise.shopping.dto.OrderDto;
import com.exercise.shopping.dto.OrderRequestDto;

import java.util.List;

public interface IOrderService {

    List<OrderDto> list();

    List<OrderDto> listBetweenDates(long from, long to);

    long create(OrderRequestDto dto);

}