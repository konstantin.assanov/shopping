package com.exercise.shopping.validation.impl;

import com.exercise.shopping.exceptions.BadRequestException;
import com.exercise.shopping.validation.IBasicValidator;
import org.springframework.stereotype.Service;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class BasicValidator<T> implements IBasicValidator<T> {

    private final Validator validator;

    BasicValidator(Validator validator) {
        this.validator = validator;
    }

    private List<String> _validate(T data) {
        return validator
                .validate(data)
                .stream()
                .map(ConstraintViolation::getMessage)
                .collect(Collectors.toList());
    }

    public void validate(T data) throws BadRequestException {
        final List<String> errors = _validate(data);
        if (!errors.isEmpty()) {
            throw new BadRequestException(errors);
        }
    }

}