package com.exercise.shopping.validation;

public interface IBasicValidator<T> {

    void validate(T order);

}
