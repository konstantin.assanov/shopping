# Basic Product management and Order placement

## Overview

Product:
- Create a new product
- Retrieve a list of all products
- Update a product
- Delete a product (soft deletion)
Each product has a SKU (unique id), a name, a price and date it was created.

Order:
- Place an order
- Retrieve all orders within a given time period
Each order has a list of product, unique id, the buyer’s e-mail, and the time the
order was placed. It's possible to calculate the total order amount, based on the
price of the individual products.

## 0. Introduction

* **Swagger specification** is available at the root of the project [./swagger.yaml](./swagger.yaml). 
* **Build jar**: mvn clean package
* **Launch server**: java -jar target/shopping-0.0.1-SNAPSHOT.jar
* **Check Swagger-UI**: http://localhost:8080/swagger-ui.html
* **spingdoc-openapi** OpenAPI 3 is included into distribution, so it's
possible to observe the API and execute the requests directly with UI. 

## 1. Packages

* **controllers** - controllers
* **dto** - DTO objects
* **entities** - Entities
* **exceptions** - custom exception classes
* **services** - basic services' interfaces
* **services.converters** - entity to DTO converters
* **services.impl** - services implementing the interfaces   
* **store** - persistence interfaces
* **store.impl** - persistence interface implementation
* **store.repository** - CRUD repositories

## 2. Comments on implementation

### 2.1 Products and Orders separation

*Products* and *Orders* services are strongly separated at every level of implementation: 
indeed, *Products* and *Orders* are serving the different purposes, and their clients
may be strongly different. ISP principle would require to expose
the separate interfaces for Products and Orders.

It's easy to imagine that Products and Orders management would go to the separate microservices:
indeed, the *Products* service would belong to a ***Catalog***, and the *Orders* would belong
to ***Checkout***. Generally, these are very different domains - ***Checkout*** services are
exposed to the purchasers, and it presumes quite frequent data updates, while ***Catalog*** is
not updated so frequently, and these updates are coming from the other type of organizations.

So, at every implementation level *Products* and *Orders* services are implemented separately
and independently.

It would be possible to put these classes into separate domain-related packages - products.* and
orders.*. The given implementation does not put them into packages, following the domain-related logic,
for the sake of simplicity, but the product-related and order-related classes are completely independent,
except the persistence management (that however can be also easily separated if the data sources for products 
and for orders are different also).          

By other words, products and orders implementation may be put into the separate microservices 
with no pain.
  
### 2.2 Order-Product Many-to-Many mapping

*Order* entity makes a direct reference to *Product* entity to define many-to-many mapping, and
it's the only explicit dependency between these two otherwise separated domains. 

### 2.3 DTOs and Entities

I use here DTOs separately from Entities. DTOs are for communication with the service clients, and
Entities are for persistence. 

DTOs are also read-only and potentially shareable with the service clients, and Entities may contain the information
that is internal to the service. For example, Product entity contains the 'isDeleted' field to implement
the 'soft delete', and this field is never exposed in DTO.

Also, because of the strong separation between controllers, services and stores, *controllers*
operate with DTOs only, while *stores* process Entities only. 

### 2.4 Dependency Injection and DIP

*Services* from the **service** package and *Stores* from **store** package are exposing the interfaces
of its methods instead of providing the client classes with the implementing classes directly. Here
the framework executes the dependency injection, based on the interfaces. This allows to better decouple
*controllers* from *services* classes and *services* from *stores* classes, where
*controllers* are responsible of handling the web requests, *services* are about implementing
the business logic, and *stores* are for managing the persistence.   

Here I make the controllers very light, pushing most of operations into the services.

I also introduced the *store* services to isolate the client services from the specifics of persistence
implementation, as it's a part that bears a quite important risk of change. For example, by moving
from sql-based storage to key-value storage, or even by delegating the storage to other microservice.

### 2.5 Validation

In this implementation the validation of the submitted data happens with the help of Entity validation
only; but it would be good also to add the request DTO validation later.

### 2.6 Total order price calculation and retrieval of the product ids of an order

I implemented total order price calculation directly as a method of the *Order* Entity, because
it's intrinsically linked to the structure of the order. Nevertheless, it implicitly relies 
on the mapping between the *Order* and the *Products*, and, consequently, on the Hibernate layer.

If Orders and Products are managed by the separate microservices, such methods would be better moved
to a separate service, which would manage the retrieval of the prices from a *products* microservice.          
  
### 2.7 Some Details

* **price** is an integer and indicates the price in cents
* **timestamp** - time of creation and time of placement are respesented by timestamp    
* **products of an Order** - validation of a submitted order in what concerns its set of products is made
 quite resistant - duplicates are permitted (and further eliminated), and the non-existing product ids are simply rejected
 without producing an error. Empty product lists are not tolerated however. 
 
### 2.8 Integration Tests with MockMvc

Integrations Tests with MockMvc realise 2 scenarios to test (1) with the sequence of operations for the Products and
(2) with the sequence of operations for Orders.  
 
## 3. Profiles
 
### 3.1 Profiles
 
 The application.yml contains three profiles: 'dev', 'test' and 'prod' which differentiate by description of the data source used:
 * **'dev'** uses local H2 database;
 * **'test'** uses local PostgreSQL database with a pre-created 'shopping_test' database;
 * **'prod'** uses PostgreSQL database but the Server has to be defined with the environment variables on application launch.
 
 The **'prod'** profile does not define the credentials for access to the database. These credentials should not be stored in version control
 system, but should be passed at deployment as, for example, environment variables.
 
### 3.2 Build and Run
 
 To build: 'mvn clean package'
 
 To run:
  * **'dev'**: 
  ```
  java -jar target/shopping-0.0.1-SNAPSHOT.jar 
  ```      
  * **'test'**: 
  ```  
  java -jar target/shopping-0.0.1-SNAPSHOT.jar --spring.profiles.active=test
  ```
  * **'prod'**: 
  ```
  java -jar target/shopping-0.0.1-SNAPSHOT.jar --spring.profiles.active=prod --DB_SERV=<server IP:port> --DB_USR=<user> --DB_PWD=<password>
  ```
 
